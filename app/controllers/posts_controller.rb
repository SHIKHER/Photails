class PostsController < ApplicationController
  before_action :set, only: [:show, :edit, :update, :destroy, :like]
  before_action :authenticate_user!
  before_action :owned, only: [:edit, :update, :destroy]

  def index
    @posts = Post.all.order('created_at DESC').page params[:page]
  end

  def new
    @post = current_user.posts.build
  end

  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:success]="post created"
      redirect_to posts_path
    else
      flash.now[:error]= "not created"
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @post.update(post_params)
      flash[:success] = "updated"
      redirect_to posts_path(@post)
    else
      flash.now[:error]="not updated"
      render :edit
    end
  end

  def destroy
    @post.destroy
    redirect_to posts_path
  end

  def like
    if @post.liked_by current_user
      respond_to do |format|
        format.html { redirect_to :back }
        format.js
      end
    end
  end





  private

  def post_params
    params.require(:post).permit(:caption, :image)
  end

  def set
    @post = Post.find(params[:id])
  end

  def owned
    unless current_user == @post.user
      flash[:alert] = "not allowed"
      redirect_to root_path
    end
  end

end
