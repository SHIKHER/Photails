class Post < ApplicationRecord
  acts_as_votable
  validates :image, presence: true
  has_attached_file :image, styles: { medium: "640x200", thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  validates :caption, length: {minimum: 3, maximum: 300}
  belongs_to :user
  has_many :comments, dependent: :destroy

end
